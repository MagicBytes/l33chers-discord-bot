// define imports
const axios = require('axios');
const util = require('util');
const bot_config = require("./config.json");

// func: get the global rank of the team
exports.getTeamGlobalRank = function() {
    return new Promise((resolve, reject) => {
        axios.get('https://www.hackthebox.com/api/v4/rankings/team/ranking_bracket', {
            headers: {
                'Authorization': `Bearer ${bot_config.HTB_API_TOKEN}`,
                'Content-Type': 'application/json',
                'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/37.0.2062.94 Chrome/37.0.2062.94 Safari/537.36'
            },
        })
        .then(function (response) {
            resolve(JSON.stringify(response.data.data.rank));
        })
        .catch(function (error) {
            reject(error);
        });
    });
};

// func: get the overall best global rank of the team
exports.getTeamBestRank = function() {
    return new Promise((resolve, reject) => {
        axios.get('https://www.hackthebox.com/api/v4/rankings/team/best?period=1Y', {
            headers: {
                'Authorization': `Bearer ${bot_config.HTB_API_TOKEN}`,
                'Content-Type': 'application/json',
                'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/37.0.2062.94 Chrome/37.0.2062.94 Safari/537.36'
            },
        })
        .then(function (response) {
            resolve(JSON.stringify(response.data.data.rank));
        })
        .catch(function (error) {
            reject(error);
        });
    });
};

// func: get all team members
exports.getTeamMembers = function() {
    return new Promise((resolve, reject) => {
        axios.get('https://www.hackthebox.com/api/v4/team/members/5561', {
            headers: {
                'Authorization': `Bearer ${bot_config.HTB_API_TOKEN}`,
                'Content-Type': 'application/json',
                'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/37.0.2062.94 Chrome/37.0.2062.94 Safari/537.36'
            },
        })
        .then(function (response) {
            const teamMembers = response.data.map(member => `- ${member.name}`);
            const formattedList = `Current Team members:\n${teamMembers.join('\n')}`;
            resolve(formattedList);
        })
        .catch(function (error) {
            reject(error);
        });
    });
};
