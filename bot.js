const Discord = require("discord.js");
const config = require("./config.json");
const htb = require('./htb')

const { Client, GatewayIntentBits } = require('discord.js');

const client = new Client({
	intents: [
		GatewayIntentBits.Guilds,
		GatewayIntentBits.GuildMessages,
		GatewayIntentBits.MessageContent,
		GatewayIntentBits.GuildMembers,
	],
});

const prefix = "!";

const showHelp = function() {
    var help = "`!help` - display this help section\n";
    help += "`!ping` - send a ping, get a pong\n";
    help += "`!teamrank` - get global team rank\n";
    help += "`!bestrank` - get best team rank during the year\n";
    help += "`!members` - get the team members";
    return help;
}

client.on("messageCreate", async function(message) {
    if (message.author.bot) return;
    if (!message.content.startsWith(prefix)) return;

    const commandBody = message.content.slice(prefix.length);
    const args = commandBody.split(' ');
    const command = args.shift().toLowerCase();

    if (command === "help") {
        message.reply(showHelp())
    } else if (command === "ping") {
        const timeTaken = Date.now() - message.createdTimestamp;
        message.reply(`Pong! This message had a latency of ${timeTaken}ms.`);
    } else if (command === "teamrank") {
        try {
            const rankData = await htb.getTeamGlobalRank();
            message.reply(`l33chers is currenly on Global Rank ${rankData}`);
        } catch (error) {
            console.error(error);
            message.reply("An error occurred while fetching the team rank.");
        }
    } else if (command === "bestrank") {
        try {
            const rankData = await htb.getTeamBestRank();
            message.reply(`l33chers's best Global Rank is ${rankData}`);
        } catch (error) {
            console.error(error);
            message.reply("An error occurred while fetching the team rank.");
        }
    } else if (command === "members") {
        try {
            const members = await htb.getTeamMembers();
            message.reply(members);
        } catch (error) {
            console.error(error);
            message.reply("An error occurred while fetching the team members.");
        }
    }
});
                                     


client.login(config.BOT_TOKEN);